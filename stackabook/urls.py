from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

import settings

urlpatterns = [
    # Examples:
    # url(r'^$', 'stackabook.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^v1/', include('webapi.urls', namespace='v1')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
