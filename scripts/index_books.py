import django
django.setup()

from webapi.models import Book

import pycps
con = pycps.Connection('tcp://cloud-us-0.clusterpoint.com:9007', 'books', 'bizkut@gmail.com', 'angelhack', '100407')

print "Indexing places to clusterpoint.."

count = 0

books = Book.objects.all()
sent = 0
batch = {}

for book in books:
    batch[book.id] = {
        'name': book.meta.title,
        'cover': book.meta.cover.url,
        'author': book.meta.author,
        'publisher': book.meta.publisher,
        'isbn': book.meta.publisher,
        'category': book.meta.category,
        'desc': book.meta.desc,
        'owner': book.owner.name,
        'location': book.owner.about
    }

    if len(batch) == 1000:
        try:
            sent += 1000
            print "Sending " + str(sent) + "th book"
            con.insert(batch)
            batch = {}
        except pycps.APIError as e:
            print e.message

print "Sending " + str(sent+1000) + "th book"
con.insert(batch)
batch = {}

print "Done!"