import requests
from bs4 import BeautifulSoup
import urllib
import django
from django.core.files import File
django.setup()

from webapi.models import Member, Place


page = 1
while True:
    print "Getting page: " + str(page)
    r = requests.get('http://www.hackathon.io/angelhack-kuala/people?page=' + str(page))
    soup = BeautifulSoup(r.content)
    users_html = soup.find_all('div', class_="row network_cell")

    for user_html in users_html:
        # user's name
        name = user_html.find_all('a')[1].string

        # photo url
        photo_url = user_html.find_all('a')[0]['style'].split("'")[1]

        count = Member.objects.filter(name=name).count()
        if count == 0:
            member = Member()
            member.name = name
            try:
                content = urllib.urlretrieve(photo_url)
            except:
                photo_url = "http://gravatar.com/avatar/59428e23260755bf8990fc73fcfadc93.png?s=200&d=http://www.hackathon.io/assets/icns/avatar/user.png"
                content = urllib.urlretrieve(photo_url)

            fname = ''.join(e for e in name if e.isalnum()).lower() + '.jpg'
            try:
                member.photo.save(fname, File(open(content[0])), save=True)
            except:
                print "error saving photo: " + name

            member.about = Place.objects.order_by('?').first().name

            member.save()

            print "member: " + name + " added"

    if len(users_html) == 0:
        break
    page += 1