import django
django.setup()

from webapi.models import Place

import pycps
con = pycps.Connection('tcp://cloud-us-0.clusterpoint.com:9007', 'places', 'bizkut@gmail.com', 'angelhack', '100407')

print "Indexing places to clusterpoint.."

count = 0

places = Place.objects.all()
sent = 0
batch = {}

for place in places:
    batch[place.id] = {
        'name': place.name,
        'coords': {
            'lat': str(place.lat),
            'lng': str(place.lng)
        }
    }

    if len(batch) == 1000:
        try:
            sent += 1000
            print "Sending " + str(sent) + "th place"
            con.insert(batch)
            batch = {}
        except pycps.APIError as e:
            print e.message

print "Sending " + str(sent+1000) + "th place"
con.insert(batch)
batch = {}

print "Done!"