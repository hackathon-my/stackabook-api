import django
django.setup()

from webapi.models import Place


with open('../data/MY.txt') as f:

    docs = {}
    print "Importing places.."
    for line in f.readlines():
        data = line.split('\t')
        id = int(data[0] + "0")
        name = data[2]
        lat = data[4]
        lng = data[5]
        code = data[6]
        if code == 'P':
            count = Place.objects.filter(name=name).count()
            if count == 0:
                place = Place()
                place.name = name
                place.lat = lat
                place.lng = lng
                place.save()
    print "Done!"