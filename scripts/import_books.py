from random import randint

from googleapiclient.discovery import build
import urllib
import django
from django.core.files import File
django.setup()

from webapi.models import BookMeta, Book, Member


keywords = ['java', 'cisco', 'leadership', 'finance', 'architecture', 'interior design',
            'islam', 'fitness']

COUNT = 40
MAX = 80
START = 0


books_service = build('books', 'v1', developerKey='AIzaSyA26UmO6na5_PnwqHuDLRNJDzbztDoLMUM').volumes()

for kw in keywords:
    start = START
    while start + COUNT <= MAX:
        print "Fetching #" + kw + " from:" + str(start) + " to:" + str(start+COUNT)
        results = books_service.list(q=kw, startIndex=start, maxResults=COUNT).execute()
        start += COUNT

        for res in results['items']:
            id =  res['id']
            info = res['volumeInfo']
            isbn = None
            for id in info.get('industryIdentifiers', []):
                if id['type'] == 'ISBN_13':
                    isbn = id['identifier']
            if isbn is None:
                continue

            count = BookMeta.objects.filter(isbn=isbn).count()
            if count == 0:
                try:
                    meta = BookMeta()
                    meta.title = info['title']
                    meta.author = ','.join(info['authors'])
                    meta.publisher = info['publisher']
                    meta.desc = info['description']
                    meta.isbn = isbn
                    meta.category = info['categories'][0]
                    meta.retail_price = res['saleInfo']['listPrice']['amount']
                    photo = urllib.urlretrieve(info['imageLinks']['thumbnail'])
                    fname = isbn + '.jpg'
                    meta.cover.save(fname, File(open(photo[0])), save=True)
                    meta.save()

                    owner = Member.objects.order_by('?').first()

                    book = Book()
                    book.meta = meta
                    book.owner = owner

                    pcent = randint(1, 20) / 100.0
                    rate = float(meta.retail_price) * pcent
                    book.rate = str(rate)
                    book.save()

                except Exception, e:
                    print e.message


