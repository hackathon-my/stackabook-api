from django.contrib import admin
from webapi import models

# Register your models here.

admin.site.register(models.Member)
admin.site.register(models.BookMeta)
admin.site.register(models.Book)
admin.site.register(models.BookPhoto)
admin.site.register(models.Rental)
admin.site.register(models.Chat)
