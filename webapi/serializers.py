from collections import OrderedDict

from rest_framework import serializers

import models

class RecursiveField(serializers.Serializer):
    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data


class MemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Member

class BookMetaSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BookMeta

class BookSerializer(serializers.ModelSerializer):
    owner = MemberSerializer(read_only=True)
    meta = BookMetaSerializer(read_only=True)

    class Meta:
        model = models.Book



class BookSerializer2(serializers.BaseSerializer):

    def __init__(self, instance=None, data=None, request=None,  **kwargs):
        self.request = request
        super(BookSerializer2, self).__init__(**kwargs)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        print validated_data

    def to_internal_value(self, data):
        pass

    def to_representation(self, book):
        scheme = 'https://' if self.request.is_secure() else 'http://'
        host = scheme + self.request.get_host() + '/media/'

        resp = OrderedDict()
        resp['id'] = book.id
        resp['meta_id'] = book.meta.id
        resp['title'] = unicode(book.meta.title)
        resp['cover'] = host + str(book.meta.cover)
        resp['owner'] = unicode(book.owner.name)
        resp['rate'] = unicode(book.rate)
        return resp