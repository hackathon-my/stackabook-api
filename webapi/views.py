import json

from rest_framework import viewsets, status
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response

import models
import serializers

import pycps
con = pycps.Connection('tcp://cloud-us-0.clusterpoint.com:9007', 'places', 'bizkut@gmail.com', 'angelhack', '100407')
con_books = pycps.Connection('tcp://cloud-us-0.clusterpoint.com:9007', 'books', 'bizkut@gmail.com', 'angelhack', '100407')



class MemberViewSet(viewsets.ModelViewSet):

    queryset = models.Member.objects.all()
    serializer_class = serializers.MemberSerializer


class BookViewSet(viewsets.ModelViewSet):

    queryset = models.Book.objects.all()
    serializer_class = serializers.BookSerializer

    @list_route(methods=['get'])
    def search2(self, request):
        q = request.GET.get('q')
        place = request.GET.get('loc')
        books = models.Book.objects.filter(owner__about__contains=place)
        ser = serializers.BookSerializer(books)
        return Response(ser.data)

    @list_route(methods=['get'])
    def search(self, request):
        query = request.GET.get('q', '')
        docs = con_books.search(query, docs=200)
        items = []
        data = docs.get_documents()
        if data:
            for doc in data.values():
                items.append(doc)
        return Response({'count': docs.hits, 'items': items})


    # def get_object(self, id):
    #     try:
    #         book = models.Book.objects.get(id=id)
    #         return book
    #     except models.Book.DoesNotExist:
    #         return None
    #
    # def retrieve(self, request, *args, **kwargs):
    #     # isbn = request.GET.get('isbn')
    #     id = kwargs.get('pk')
    #     object = self.get_object(id)
    #     if object:
    #         ser = serializers.BookSerializer(object, request=request)
    #         return Response(ser.to_representation(object))
    #     else:
    #         return Response(status=status.HTTP_400_BAD_REQUEST)
    #
    # def list(self, request):
    #     query = request.GET.get('isbn', '')
    #     docs = con.search(query + '*', docs=50)
    #     out = []
    #     data = docs.get_documents()
    #     if data:
    #         for doc in data.values():
    #             out.append(doc['name'])
    #     return Response(out)
    #
    #
    # def create(self, request, *args, **kwargs):
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     self.perform_create(serializer)
    #     return Response(serializer.data, status=status.HTTP_201_CREATED)
    #
    # def perform_create(self, serializer):
    #     serializer.save()




class BookMetaViewSet(viewsets.ModelViewSet):
    """
    API endpoint for getting book meta.
    """
    queryset = models.BookMeta.objects.all()
    serializer_class = serializers.BookMetaSerializer
    filter_fields = ('isbn',)

    def get_object(self, isbn):
        try:
            book = models.BookMeta.objects.get(isbn=isbn)
            return book
        except models.BookMeta.DoesNotExist:
            return None

    def retrieve(self, request, *args, **kwargs):
        # isbn = request.GET.get('isbn')
        isbn = kwargs.get('pk')
        object = self.get_object(isbn)
        if object:
            ser = serializers.BookMetaSerializer(object)
            return Response(ser.data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    # @list_route(methods=['post'])
    # def addmeta(self, request):
    #     client_id = request.POST.get('client_id')
    #     ad_id = request.POST.get('ad_id')
    #     add_count = request.POST.get('count', 0)
        # if not client_id or not ad_id:
        #     return Response(status=status.HTTP_400_BAD_REQUEST)
        # try:
        #     impression = Impression.objects.get(client=client_id, ad=ad_id)
        #     impression.impression_count += int(add_count)
        #     impression.save()
        #     serializer = ImpressionSerializer(impression)
        #     return Response(serializer.data)
        # except Impression.DoesNotExist:
        #     impression = Impression()
        #     impression.client_id = client_id
        #     impression.ad_id = ad_id
        #     impression.impression_count = add_count
        #     try:
        #         impression.save()
        #         serializer = ImpressionSerializer(impression)
        #         return Response(serializer.data)
        #     except IntegrityError:
        #         return Response(status=status.HTTP_400_BAD_REQUEST)


class RecentBookViewSet(viewsets.ViewSet):
    """
    API endpoint for recent books added.
    """

    def list(self, request):
        queryset = models.Book.objects.order_by('?')[:10]
        serializer = serializers.BookSerializer2(queryset, request=request, many=True)
        return Response(serializer.data)


class PlaceSuggestViewSet(viewsets.ViewSet):
    """
    API endpoint for place name suggester.
    """

    def list(self, request):
        query = request.GET.get('q', '')
        docs = con.search(query + '*', docs=50)
        out = []
        data = docs.get_documents()
        if data:
            for doc in data.values():
                out.append(doc['name'])
        return Response(out)
