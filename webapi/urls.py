from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

import views


router = DefaultRouter(trailing_slash=False)
router.register(r'members', views.MemberViewSet)
router.register(r'books', views.BookViewSet, base_name='books')
router.register(r'metas', views.BookMetaViewSet)
router.register(r'recentbooks', views.RecentBookViewSet, base_name='recentbooks')
router.register(r'placesuggest', views.PlaceSuggestViewSet, base_name='placesuggest')


urlpatterns = [
    url(r'^', include(router.urls, namespace='v1')),
]