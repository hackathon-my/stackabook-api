from django.db import models

# Create your models here.

class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Place(BaseModel):
    name = models.CharField(max_length=250)
    lat = models.FloatField()
    lng = models.FloatField()


class Member(BaseModel):
    name = models.CharField(max_length=50)
    about = models.TextField()
    photo = models.ImageField(upload_to='member/photos')

    def __unicode__(self):
        return self.name


class BookMeta(BaseModel):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    publisher = models.CharField(max_length=100)
    isbn = models.CharField(max_length=20)
    cover = models.ImageField(upload_to='book/covers')
    retail_price = models.CharField(max_length=50)
    desc = models.TextField(null=True)
    category = models.CharField(max_length=50, null=True)

    def __unicode__(self):
        return self.title


class Book(BaseModel):
    meta = models.ForeignKey(BookMeta)
    desc = models.TextField(null=True)
    owner = models.ForeignKey(Member)
    rate = models.TextField()

    def __unicode__(self):
        return self.meta.title


class BookPhoto(BaseModel):
    book = models.ForeignKey(Book)
    photo = models.ImageField(upload_to='book/photos')


class Rental(BaseModel):
    renter = models.ForeignKey(Member, related_name='renter')
    rentee = models.ForeignKey(Member, related_name='rentee')
    book = models.ForeignKey(Book)
    rent_at = models.DateField(null=True, blank=True)
    return_at = models.DateField(null=True, blank=True)


class Chat(BaseModel):
    rental = models.ForeignKey(Rental)
    member = models.ForeignKey(Member)
    text = models.TextField()

    def __unicode__(self):
        return u"{0}: {1}".format(self.member.name, self.text)
